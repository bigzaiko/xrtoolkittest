﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class Handtracking : MonoBehaviour
{
    public Hand hand;
    public Bone root;

    List<Bone> bones = new List<Bone>();

    Vector3 thumbPosition = Vector3.zero;

    private void Start()
    {
        var inputFeatures = new List<InputFeatureUsage>();

        var inputDevices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.HandTracking, inputDevices);

        hand.TryGetRootBone(out root);
        bool thumb = hand.TryGetFingerBones(HandFinger.Thumb, bones);

        Debug.Log("Bone: " + thumb + " _ " + thumbPosition);
        foreach (var bone in bones)
        {
            bone.TryGetPosition(out thumbPosition);
            Debug.Log("Bone: " + bone + " _ " + thumbPosition);
        }

        foreach (var device in inputDevices)
        {
            if (device.TryGetFeatureUsages(inputFeatures))
            {

                Debug.Log(string.Format("Bool feature {0}'s value is {1}", device.characteristics.ToString(), "feature"));


            }
        }
    }
    void Update()
    {
        
    }
}
